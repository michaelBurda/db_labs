<?php

namespace app\modules\student\models\search;

use app\modules\group\models\Group;
use app\modules\progress\models\Progress;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\student\models\Student;

/**
 * StudentSearch represents the model behind the search form of `app\modules\student\models\Student`.
 */
class StudentSearch extends Student
{
    public $fullName;
    public $groupName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['last_name', 'first_name', 'middle_name', 'groupName', 'form', 'fullName', 'scholarship'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tn_student = Student::tableName();
        $tn_group = Group::tableName();

        $query = Student::find()
            ->from("$tn_student as s")
            ->leftJoin("$tn_group as g", 'g.id = s.group_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => ['s.id' => SORT_ASC],
                    'desc' => ['s.id' => SORT_DESC],
                ],
                'fullName' => [
                    'asc' => ['s.last_name' => SORT_ASC, 's.first_name' => SORT_ASC, 's.middle_name' => SORT_ASC],
                    'desc' => ['s.last_name' => SORT_DESC, 's.first_name' => SORT_DESC, 's.middle_name' => SORT_DESC],
                    'label' => 'Name',
                    'default' => SORT_ASC
                ],
                'groupName' => [
                    'asc' => ['g.name' => SORT_ASC],
                    'desc' => ['g.name' => SORT_DESC],
                ],
                'scholarship' => [
                    'asc' => ['s.scholarship' => SORT_ASC],
                    'desc' => ['s.scholarship' => SORT_DESC],
                ],
                'form' => [
                    'asc' => ['s.form' => SORT_ASC],
                    'desc' => ['s.form' => SORT_DESC],
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if (!empty($this->fullName)) {
            $query->andFilterWhere(['like', 's.first_name', $this->fullName])
                ->orFilterWhere(['like', 's.last_name', $this->fullName])
                ->orFilterWhere(['like', 's.middle_name', $this->fullName]);
        }

        if (!empty($this->groupName)) {
            $query->andFilterWhere(['like', 'g.name', $this->groupName]);
        }

        if (!empty($this->scholarship)) {
            if ($c = $this->isComparable($this->scholarship)) {
                $query->andWhere([$c['condition'], 's.scholarship', $c['value']]);
            } else {
                $query->andWhere(['s.scholarship' => $this->scholarship]);
            }
        }

        $query->andFilterWhere([
            's.id' => $this->id
        ]);

        $query->andFilterWhere(['like', 's.group', $this->group])
            ->andFilterWhere(['like', 's.form', $this->form]);

        return $dataProvider;
    }

    private function isComparable($string) {
        $conditions = ['>=', '<=', '>', '<'];
        foreach ($conditions as $condition) {
            if (strpos($string, $condition) === 0) {
                return [
                    'condition' => $condition,
                    'value' => str_replace($condition, '', $string)
                ];
            }
        }
        return false;
    }
}
