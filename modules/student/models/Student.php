<?php

namespace app\modules\student\models;

use app\modules\progress\models\Progress;
use Yii;
use app\modules\group\models\Group;

/**
 * This is the model class for table "{{%student}}".
 *
 * @property int $id
 * @property string $last_name
 * @property string $first_name
 * @property string $middle_name
 * @property string $group_id
 * @property double $scholarship
 * @property string $form
 */
class Student extends \yii\db\ActiveRecord
{
     /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%student}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['scholarship', 'group_id'], 'number'],
            [['last_name', 'first_name', 'middle_name', 'form'], 'string', 'max' => 30],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Group::className(), 'targetAttribute' => ['group_id' => 'id']],
        ];
    }

    public function fields()
    {
        return [
            'fullName',
            'averageMark',
            'groupName'
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'last_name' => 'Last Name',
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'group_id' => 'Group ID',
            'scholarship' => 'Scholarship',
            'form' => 'Form',
            'fullName' => 'Full name'
        ];
    }

    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    public function getGroupName()
    {
        return ($this->group !== null) ? $this->group->name : null;
    }

    public function getFullName()
    {
        return $this->last_name . ' ' . $this->first_name . ' ' . $this->middle_name;
    }

    public function getAverageMark()
    {
        $averageMark = Progress::find()
            ->where(['student_id' => $this->id])
            ->average('mark');

        return round($averageMark, 1);
    }
}
