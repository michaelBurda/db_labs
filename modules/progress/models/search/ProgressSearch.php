<?php

namespace app\modules\progress\models\search;

use app\modules\student\models\Student;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\progress\models\Progress;
use app\modules\subject\models\Subject;

/**
 * ProgressSearch represents the model behind the search form of `app\modules\progress\models\Progress`.
 */
class ProgressSearch extends Progress
{
    public $studentFullName;
    public $subjectName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'student_id', 'subject_id'], 'integer'],
            [['date', 'studentFullName', 'subjectName', 'mark'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tn_subject = Subject::tableName();
        $tn_progress = Progress::tableName();
        $tn_student = Student::tableName();

        $query = Progress::find()
            ->from("$tn_progress as p")
            ->leftJoin("$tn_student as st", 'st.id = p.student_id')
            ->leftJoin("$tn_subject as sb", 'sb.id = p.subject_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => ['s.id' => SORT_ASC],
                    'desc' => ['s.id' => SORT_DESC],
                ],
                'studentFullName' => [
                    'asc' => ['st.last_name' => SORT_ASC, 'st.first_name' => SORT_ASC, 'st.middle_name' => SORT_ASC],
                    'desc' => ['st.last_name' => SORT_DESC, 'st.first_name' => SORT_DESC, 'st.middle_name' => SORT_DESC],
                ],
                'subjectName' => [
                    'asc' => ['sb.name' => SORT_ASC],
                    'desc' => ['sb.name' => SORT_DESC],
                ],
                'date' => [
                    'asc' => ['p.date' => SORT_ASC],
                    'desc' => ['p.date' => SORT_DESC],
                ],
                'mark' => [
                    'asc' => ['p.mark' => SORT_ASC],
                    'desc' => ['p.mark' => SORT_DESC],
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if (!empty($this->studentFullName)) {
            $query->andFilterWhere(['like', 'st.first_name', $this->studentFullName])
                ->orFilterWhere(['like', 'st.last_name', $this->studentFullName])
                ->orFilterWhere(['like', 'st.middle_name', $this->studentFullName]);
        }

        if (!empty($this->subjectName)) {
            $query->andFilterWhere(['like', 'sb.name', $this->subjectName]);
        }

        if (!empty($this->mark)) {
            if ($c = $this->isComparable($this->mark)) {
                $query->andWhere([$c['condition'], 'p.mark', $c['value']]);
            } else {
                $query->andWhere(['p.mark' => $this->mark]);
            }
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
        ]);

        return $dataProvider;
    }

    private function isComparable($string) {
        $conditions = ['>=', '<=', '>', '<'];
        foreach ($conditions as $condition) {
            if (strpos($string, $condition) === 0) {
                return [
                    'condition' => $condition,
                    'value' => str_replace($condition, '', $string)
                ];
            }
        }
        return false;
    }
}
