<?php

namespace app\modules\progress\models;

use Yii;
use app\modules\subject\models\Subject;
use app\modules\student\models\Student;

/**
 * This is the model class for table "{{%progress}}".
 *
 * @property int $id
 * @property string $date
 * @property int $student_id
 * @property int $subject_id
 * @property int $mark
 *
 * @property Subject $subject
 * @property Student $student
 */
class Progress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%progress}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['student_id', 'subject_id', 'mark'], 'integer'],
            [['subject_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subject::className(), 'targetAttribute' => ['subject_id' => 'id']],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_id' => 'id']],
        ];
    }

    public function fields()
    {
        return [
            'studentFullName',
            'subjectName'
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'student_id' => 'Student ID',
            'subject_id' => 'Subject ID',
            'mark' => 'Mark',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }

    public function getSubjectName()
    {
        return ($this->subject !== null) ? $this->subject->name : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['id' => 'student_id']);
    }

    public function getStudentFullName()
    {
        return ($this->student !== null) ? $this->student->fullName : null;
    }
}
