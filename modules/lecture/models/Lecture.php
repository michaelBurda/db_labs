<?php

namespace app\modules\lecture\models;

use Yii;

/**
 * This is the model class for table "{{%lecture}}".
 *
 * @property int $id
 * @property string $last_name
 * @property string $first_name
 * @property string $middle_name
 * @property string $department
 * @property string $role
 * @property double $salary
 */
class Lecture extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%lecture}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['salary'], 'number'],
            [['last_name', 'first_name', 'middle_name'], 'string', 'max' => 30],
            [['department', 'role'], 'string', 'max' => 80],
        ];
    }

    public function fields()
    {
        return [
            'fullName'
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'last_name' => 'Last Name',
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'department' => 'Department',
            'role' => 'Role',
            'salary' => 'Salary',
            'fullName' => 'Full name'
        ];
    }

    public function getFullName()
    {
        return $this->last_name . ' ' . $this->first_name . ' ' . $this->middle_name;
    }
}
