<?php

namespace app\modules\lecture\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\lecture\models\Lecture;

/**
 * LectureSearch represents the model behind the search form of `app\modules\lecture\models\Lecture`.
 */
class LectureSearch extends Lecture
{
    public $fullName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['last_name', 'first_name', 'middle_name', 'department', 'role', 'fullName', 'salary'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tn_lecture = Lecture::tableName();

        $query = Lecture::find()
            ->from("$tn_lecture as l");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => ['l.id' => SORT_ASC],
                    'desc' => ['l.id' => SORT_DESC],
                ],
                'fullName' => [
                    'asc' => ['l.last_name' => SORT_ASC, 'l.first_name' => SORT_ASC, 'l.middle_name' => SORT_ASC],
                    'desc' => ['l.last_name' => SORT_DESC, 'l.first_name' => SORT_DESC, 'l.middle_name' => SORT_DESC],
                    'label' => 'Name',
                    'default' => SORT_ASC
                ],
                'department' => [
                    'asc' => ['l.department' => SORT_ASC],
                    'desc' => ['l.department' => SORT_DESC],
                ],
                'role' => [
                    'asc' => ['l.role' => SORT_ASC],
                    'desc' => ['l.role' => SORT_DESC],
                ],
                'salary' => [
                    'asc' => ['l.salary' => SORT_ASC],
                    'desc' => ['l.salary' => SORT_DESC],
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if (!empty($this->fullName)) {
            $query->andFilterWhere(['like', 'l.first_name', $this->fullName])
                ->orFilterWhere(['like', 'l.last_name', $this->fullName])
                ->orFilterWhere(['like', 'l.middle_name', $this->fullName]);
        }

        if (!empty($this->salary)) {
            if ($c = $this->isComparable($this->salary)) {
                $query->andWhere([$c['condition'], 'l.salary', $c['value']]);
            } else {
                $query->andWhere(['l.salary' => $this->salary]);
            }
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'department', $this->department])
            ->andFilterWhere(['like', 'role', $this->role]);

        return $dataProvider;
    }

    private function isComparable($string) {
        $conditions = ['>=', '<=', '>', '<'];
        foreach ($conditions as $condition) {
            if (strpos($string, $condition) === 0) {
                return [
                    'condition' => $condition,
                    'value' => str_replace($condition, '', $string)
                ];
            }
        }
        return false;
    }
}
