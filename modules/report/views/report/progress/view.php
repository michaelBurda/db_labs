<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $studentModel app\modules\student\models\Student */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->title = $studentModel->fullName . ' (' . $studentModel->averageMark . ')';
$this->params['breadcrumbs'][] = 'Reports';
$this->params['breadcrumbs'][] = ['label' => 'About progress', 'url' => ['progress-list']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'subjectName',
            'mark'
        ]
    ]) ?>
</div>
