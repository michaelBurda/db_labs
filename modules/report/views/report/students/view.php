<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $groupModel app\modules\group\models\Group */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->title = $groupModel->name . ' (' . $groupModel->studentsScholarshipSum . ')';
$this->params['breadcrumbs'][] = 'Reports';
$this->params['breadcrumbs'][] = ['label' => 'About students', 'url' => ['students-list']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'fullName',
            'scholarship'
        ]
    ]) ?>
</div>