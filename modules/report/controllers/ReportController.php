<?php

namespace app\modules\report\controllers;

use app\modules\group\models\Group;
use app\modules\group\models\search\GroupSearch;
use app\modules\progress\models\Progress;
use app\modules\student\models\Student;
use yii\web\Controller;
use Yii;
use app\modules\student\models\search\StudentSearch;
use yii\data\ArrayDataProvider;

class ReportController extends Controller
{
    public function actionProgressList()
    {
        $searchModel = new StudentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('progress\list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionProgressView($id)
    {
        $gridViewDataProvider = new ArrayDataProvider([
            'allModels' => Progress::findAll(['student_id' => $id]),
        ]);

        return $this->render('progress\view', [
            'dataProvider' => $gridViewDataProvider,
            'studentModel' => Student::findOne($id)
        ]);
    }

    public function actionStudentsList()
    {
        $searchModel = new GroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('students\list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionStudentsView($id)
    {
        $gridViewDataProvider = new ArrayDataProvider([
            'allModels' => Student::findAll(['group_id' => $id]),
        ]);

        return $this->render('students\view', [
            'dataProvider' => $gridViewDataProvider,
            'groupModel' => Group::findOne($id)
        ]);
    }
}
