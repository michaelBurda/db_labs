<?php

namespace app\modules\subject\models;

use Yii;
use app\modules\lecture\models\Lecture;

/**
 * This is the model class for table "{{%subject}}".
 *
 * @property int $id
 * @property string $name
 * @property int $lecture_id
 * @property int $hours
 * @property int $term
 *
 * @property Lecture $lecture
 */
class Subject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%subject}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lecture_id', 'hours', 'term'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['lecture_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lecture::className(), 'targetAttribute' => ['lecture_id' => 'id']],
        ];
    }

    public function fields()
    {
        return [
            'lectureFullName'
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'lecture_id' => 'Lecture ID',
            'hours' => 'Hours',
            'term' => 'Term',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLecture()
    {
        return $this->hasOne(Lecture::className(), ['id' => 'lecture_id']);
    }

    public function getLectureFullName() {
        return ($this->lecture !== null) ? $this->lecture->fullName : null;
    }
}
