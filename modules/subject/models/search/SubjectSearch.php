<?php

namespace app\modules\subject\models\search;

use app\modules\lecture\models\Lecture;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\subject\models\Subject;

/**
 * SubjectSearch represents the model behind the search form of `app\modules\subject\models\Subject`.
 */
class SubjectSearch extends Subject
{
    public $lectureFullName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lecture_id'], 'integer'],
            [['name', 'hours', 'term', 'lectureFullName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tn_subject = Subject::tableName();
        $tn_lecture = Lecture::tableName();

        $query = Subject::find()
            ->from("$tn_subject as s")
            ->leftJoin("$tn_lecture as l", 'l.id = s.lecture_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => ['s.id' => SORT_ASC],
                    'desc' => ['s.id' => SORT_DESC],
                ],
                'lectureFullName' => [
                    'asc' => ['l.last_name' => SORT_ASC, 'l.first_name' => SORT_ASC, 'l.middle_name' => SORT_ASC],
                    'desc' => ['l.last_name' => SORT_DESC, 'l.first_name' => SORT_DESC, 'l.middle_name' => SORT_DESC],
                    'label' => 'Lecture name',
                    'default' => SORT_ASC
                ],
                'name' => [
                    'asc' => ['s.name' => SORT_ASC],
                    'desc' => ['s.name' => SORT_DESC],
                ],
                'hours' => [
                    'asc' => ['s.hours' => SORT_ASC],
                    'desc' => ['s.hours' => SORT_DESC],
                ],
                'term' => [
                    'asc' => ['s.term' => SORT_ASC],
                    'desc' => ['s.term' => SORT_DESC],
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if (!empty($this->lectureFullName)) {
            $query->andFilterWhere(['like', 'l.first_name', $this->lectureFullName])
                ->orFilterWhere(['like', 'l.last_name', $this->lectureFullName])
                ->orFilterWhere(['like', 'l.middle_name', $this->lectureFullName]);
        }

        if (!empty($this->hours)) {
            if ($c = $this->isComparable($this->hours)) {
                $query->andWhere([$c['condition'], 's.hours', $c['value']]);
            } else {
                $query->andWhere(['s.hours' => $this->hours]);
            }
        }

        if (!empty($this->term)) {
            if ($c = $this->isComparable($this->term)) {
                $query->andWhere([$c['condition'], 's.term', $c['value']]);
            } else {
                $query->andWhere(['s.term' => $this->term]);
            }
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    private function isComparable($string) {
        $conditions = ['>=', '<=', '>', '<'];
        foreach ($conditions as $condition) {
            if (strpos($string, $condition) === 0) {
                return [
                    'condition' => $condition,
                    'value' => str_replace($condition, '', $string)
                ];
            }
        }
        return false;
    }
}
