-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 28 2017 г., 16:36
-- Версия сервера: 5.5.53
-- Версия PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `university`
--

-- --------------------------------------------------------

--
-- Структура таблицы `group`
--

CREATE TABLE `group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `group`
--

INSERT INTO `group` (`id`, `name`) VALUES
(1, 'PM-31'),
(2, 'I-31');

-- --------------------------------------------------------

--
-- Структура таблицы `lecture`
--

CREATE TABLE `lecture` (
  `id` int(11) NOT NULL,
  `last_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middle_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `department` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salary` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `lecture`
--

INSERT INTO `lecture` (`id`, `last_name`, `first_name`, `middle_name`, `department`, `role`, `salary`) VALUES
(1, 'Ostapchuk', 'Oksana', 'Petrivna', 'Applied Math', 'Head lecture', 10000),
(2, 'Pryshchepa', 'Oksana', 'Volodymyrivna', 'Applied Math', 'Head lecture', 10000),
(3, 'Hariv', 'Natalia', 'Oleksiivna', 'Applied Math', 'Head lecture', 10000),
(4, 'Stepanchenko', 'Olga', 'Mykolaiivna', 'Applied Math', 'Head lecture', 10000),
(5, 'Cvetkova', 'Tetiana', 'Pavlivna', 'Applied Math', 'Head lecture', 10000);

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1495975431),
('m170522_060048_create_group_table', 1495975432),
('m170522_060049_create_student_table', 1495975432),
('m170522_063728_create_lecture_table', 1495975432),
('m170522_070421_create_subject_table', 1495975433),
('m170522_073424_create_progress_table', 1495975433);

-- --------------------------------------------------------

--
-- Структура таблицы `progress`
--

CREATE TABLE `progress` (
  `id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `mark` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `progress`
--

INSERT INTO `progress` (`id`, `date`, `student_id`, `subject_id`, `mark`) VALUES
(1, '2017-06-10', 1, 1, 5),
(2, '2016-12-23', 1, 2, 4),
(3, '2017-06-05', 1, 3, 5),
(4, '2016-12-20', 1, 4, 5),
(5, '2016-12-18', 1, 5, 5),
(6, '2017-06-10', 2, 1, 4),
(7, '2016-12-23', 2, 2, 4),
(8, '2017-06-05', 2, 3, 5),
(9, '2016-12-20', 2, 4, 5),
(10, '2016-12-18', 2, 5, 5),
(11, '2017-06-10', 3, 1, 5),
(12, '2016-12-23', 3, 2, 4),
(13, '2017-06-05', 3, 3, 5),
(14, '2016-12-20', 3, 4, 5),
(15, '2016-12-18', 3, 5, 5),
(16, '2017-06-10', 4, 1, 5),
(17, '2016-12-23', 4, 2, 4),
(18, '2017-06-05', 4, 3, 5),
(19, '2016-12-20', 4, 4, 4),
(20, '2016-12-18', 4, 5, 5),
(21, '2017-06-10', 5, 1, 4),
(22, '2016-12-23', 5, 2, 3),
(23, '2017-06-05', 5, 3, 5),
(24, '2016-12-20', 5, 4, 5),
(25, '2016-12-18', 5, 5, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `last_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middle_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `scholarship` float DEFAULT NULL,
  `form` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `student`
--

INSERT INTO `student` (`id`, `last_name`, `first_name`, `middle_name`, `group_id`, `scholarship`, `form`) VALUES
(1, 'Burda', 'Mykhaylo', 'Sergiyovich', 1, 1100, 'free'),
(2, 'Balbuch', 'Stanislav', 'Olegovych', 1, 1100, 'free'),
(3, 'Borovets', 'Oleksandr', 'Mykhaylovich', 1, 1100, 'free'),
(4, 'Gorbova', 'Iruna', 'Yaroslavivna', 2, 1100, 'free'),
(5, 'Khater', 'Filip', 'Elias', 1, NULL, 'free'),
(6, '123', '123', '123', NULL, 123, 'free'),
(7, '12das', 'asffas', 'afgsa', 1, 123, '1');

-- --------------------------------------------------------

--
-- Структура таблицы `subject`
--

CREATE TABLE `subject` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lecture_id` int(11) DEFAULT NULL,
  `hours` int(11) DEFAULT NULL,
  `term` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `subject`
--

INSERT INTO `subject` (`id`, `name`, `lecture_id`, `hours`, `term`) VALUES
(1, 'Chiselni metody matematychnoi fizyki', 1, 100, 2),
(2, 'Matematychna statystyka', 2, 80, 1),
(3, 'Bazy danyh', 3, 60, 2),
(4, 'Veb programuvannia', 4, 90, 1),
(5, 'Metody obchislen', 5, 70, 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `lecture`
--
ALTER TABLE `lecture`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `progress`
--
ALTER TABLE `progress`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_progress_student_id` (`student_id`),
  ADD KEY `FK_progress_subject_id` (`subject_id`);

--
-- Индексы таблицы `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_student_group_id` (`group_id`);

--
-- Индексы таблицы `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_subject_lecture_id` (`lecture_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `group`
--
ALTER TABLE `group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `lecture`
--
ALTER TABLE `lecture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `progress`
--
ALTER TABLE `progress`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT для таблицы `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `subject`
--
ALTER TABLE `subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `progress`
--
ALTER TABLE `progress`
  ADD CONSTRAINT `FK_progress_subject_id` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_progress_student_id` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `FK_student_group_id` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `subject`
--
ALTER TABLE `subject`
  ADD CONSTRAINT `FK_subject_lecture_id` FOREIGN KEY (`lecture_id`) REFERENCES `lecture` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
