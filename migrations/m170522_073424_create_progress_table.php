<?php

use yii\db\Migration;

/**
 * Handles the creation of table `progress`.
 */
class m170522_073424_create_progress_table extends Migration
{
    private $tn_progress = '{{%progress}}';
    private $tn_subject = '{{%subject}}';
    private $tn_student = '{{%student}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tn_progress, [
            'id' => $this->primaryKey(),
            'date' => $this->date(),
            'student_id' => $this->integer(11),
            'subject_id' => $this->integer(11),
            'mark' => $this->integer(11)
        ]);

        $this->addForeignKey('FK_progress_student_id', $this->tn_progress, 'student_id', $this->tn_student, 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('FK_progress_subject_id', $this->tn_progress, 'subject_id', $this->tn_subject, 'id', 'NO ACTION', 'NO ACTION');

        $progressRows = ['id', 'date', 'student_id', 'subject_id', 'mark'];
        $progress = [
            [1, '2017-06-10', 1, 1, 5],
            [2, '2016-12-23', 1, 2, 4],
            [3, '2017-06-05', 1, 3, 5],
            [4, '2016-12-20', 1, 4, 5],
            [5, '2016-12-18', 1, 5, 5],
            [6, '2017-06-10', 2, 1, 4],
            [7, '2016-12-23', 2, 2, 4],
            [8, '2017-06-05', 2, 3, 5],
            [9, '2016-12-20', 2, 4, 5],
            [10, '2016-12-18', 2, 5, 5],
            [11, '2017-06-10', 3, 1, 5],
            [12, '2016-12-23', 3, 2, 4],
            [13, '2017-06-05', 3, 3, 5],
            [14, '2016-12-20', 3, 4, 5],
            [15, '2016-12-18', 3, 5, 5],
            [16, '2017-06-10', 4, 1, 5],
            [17, '2016-12-23', 4, 2, 4],
            [18, '2017-06-05', 4, 3, 5],
            [19, '2016-12-20', 4, 4, 4],
            [20, '2016-12-18', 4, 5, 5],
            [21, '2017-06-10', 5, 1, 4],
            [22, '2016-12-23', 5, 2, 3],
            [23, '2017-06-05', 5, 3, 5],
            [24, '2016-12-20', 5, 4, 5],
            [25, '2016-12-18', 5, 5, 4]
        ];
        $this->batchInsert($this->tn_progress, $progressRows, $progress);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('FK_progress_student_id', $this->tn_progress);
        $this->dropForeignKey('FK_progress_subject_id', $this->tn_progress);

        $this->dropTable($this->tn_progress);
    }
}
