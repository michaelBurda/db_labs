<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lecture`.
 */
class m170522_063728_create_lecture_table extends Migration
{
    private $tn_lecture = '{{%lecture}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tn_lecture, [
            'id'            => $this->primaryKey(),
            'last_name'     => $this->string(30),
            'first_name'    => $this->string(30),
            'middle_name'   => $this->string(30),
            'department'    => $this->string(80),
            'role'          => $this->string(80),
            'salary'        => $this->float()
        ]);

        $lectureRows = ['id', 'last_name', 'first_name', 'middle_name', 'department', 'role', 'salary'];
        $lectures = [
            [1, 'Ostapchuk', 'Oksana', 'Petrivna', 'Applied Math', 'Head lecture', 10000],
		    [2, 'Pryshchepa', 'Oksana', 'Volodymyrivna', 'Applied Math', 'Head lecture', 10000],
		    [3, 'Hariv', 'Natalia', 'Oleksiivna', 'Applied Math', 'Head lecture', 10000],
		    [4, 'Stepanchenko', 'Olga', 'Mykolaiivna', 'Applied Math', 'Head lecture', 10000],
		    [5, 'Cvetkova', 'Tetiana', 'Pavlivna', 'Applied Math', 'Head lecture', 10000]
        ];
        $this->batchInsert($this->tn_lecture, $lectureRows, $lectures);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tn_lecture);
    }
}
