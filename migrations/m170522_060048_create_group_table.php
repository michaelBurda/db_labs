<?php

use yii\db\Migration;

/**
 * Handles the creation of table `group`.
 */
class m170522_060048_create_group_table extends Migration
{
    private $tn_group = '{{%group}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tn_group, [
            'id'    => $this->primaryKey(),
            'name'  => $this->string()
        ]);

        $groupRows = ['id', 'name'];
        $groups = [
            [1, 'PM-31'],
            [2, 'I-31'],
        ];
        $this->batchInsert($this->tn_group, $groupRows, $groups);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tn_group);
    }
}
