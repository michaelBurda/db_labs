<?php

use yii\db\Migration;

/**
 * Handles the creation of table `subject`.
 */
class m170522_070421_create_subject_table extends Migration
{
    private $tn_subject = '{{%subject}}';
    private $tn_lecture = '{{%lecture}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tn_subject, [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(50),
            'lecture_id'    => $this->integer(11),
            'hours'         => $this->integer(11),
            'term'          => $this->integer(11)
        ]);

        $this->addForeignKey('FK_subject_lecture_id', $this->tn_subject, 'lecture_id', $this->tn_lecture, 'id', 'NO ACTION', 'NO ACTION');

        $subjectRows = ['id', 'name', 'lecture_id', 'hours', 'term'];
        $subjects = [
            [1, 'Chiselni metody matematychnoi fizyki', 1, 100, 2],
		    [2, 'Matematychna statystyka', 2, 80, 1],
		    [3, 'Bazy danyh', 3, 60, 2],
		    [4, 'Veb programuvannia', 4, 90, 1],
		    [5, 'Metody obchislen', 5, 70, 1]
        ];
        $this->batchInsert($this->tn_subject, $subjectRows, $subjects);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('FK_subject_lecture_id', $this->tn_subject);

        $this->dropTable($this->tn_subject);
    }
}
