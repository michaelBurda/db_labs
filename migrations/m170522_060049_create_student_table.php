<?php

use yii\db\Migration;

/**
 * Handles the creation of table `student`.
 */
class m170522_060049_create_student_table extends Migration
{
    private $tn_student = '{{%student}}';
    private $tn_group = '{{%group}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tn_student, [
            'id'            => $this->primaryKey(),
            'last_name'     => $this->string(30),
            'first_name'    => $this->string(30),
            'middle_name'   => $this->string(30),
            'group_id'      => $this->integer(11),
            'scholarship'   => $this->float(),
            'form'          => $this->string(30)
        ]);

        $this->addForeignKey('FK_student_group_id', $this->tn_student, 'group_id', $this->tn_group, 'id', 'NO ACTION', 'NO ACTION');

        $studentRows = ['id', 'last_name', 'first_name', 'middle_name', 'group_id', 'scholarship', 'form'];
        $students = [
            [1, 'Burda', 'Mykhaylo', 'Sergiyovich', 1, 1100, 'free'],
		    [2, 'Balbuch', 'Stanislav', 'Olegovych', 1, 1100, 'free'],
		    [3, 'Borovets', 'Oleksandr', 'Mykhaylovich', 1, 1100, 'free'],
		    [4, 'Gorbova', 'Iruna', 'Yaroslavivna', 2, 1100, 'free'],
		    [5, 'Khater', 'Filip', 'Elias', 1, NULL, 'free']
        ];
        $this->batchInsert($this->tn_student, $studentRows, $students);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('FK_student_group_id', $this->tn_student);

        $this->dropTable($this->tn_student);
    }
}
